# Swagger Pretty Docs

Gradle project configured to generate HTML/PDF from Swagger JSON using Asciidoc.

## Steps

1. Paste Swagger JSON file to `swagger` folder:

    ```
    swagger/swagger.json
    ```

2. Run:

    ```
    gradlew asciidoctor
    ```